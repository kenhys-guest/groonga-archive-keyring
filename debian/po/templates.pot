# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the groonga-archive-keyring package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: groonga-archive-keyring\n"
"Report-Msgid-Bugs-To: groonga-archive-keyring@packages.debian.org\n"
"POT-Creation-Date: 2018-08-20 15:31+0900\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../templates:1001
msgid "Do you really trust keyring in groonga-archive-keyring package?"
msgstr ""

#. Type: boolean
#. Description
#: ../templates:1001
msgid ""
"groonga-archive-keyring contains keyring which is maintained by Groonga "
"project. This keyring is used to sign the Groonga repository which is hosted "
"at https://packages.groonga.org/. If you trust this keyring package, the "
"symlink file under /etc/apt/trusted.gpg.d/groonga-archive-keyring.gpg will "
"be created. This means that you trust all the repository which is signed by "
"this keyring. Thus, the Groonga repository will be treated as trusted "
"repository without warnings."
msgstr ""
